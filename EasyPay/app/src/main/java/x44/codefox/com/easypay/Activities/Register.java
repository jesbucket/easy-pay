package x44.codefox.com.easypay.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import x44.codefox.com.easypay.Fragments.FragmentCreateAccount1;
import x44.codefox.com.easypay.Fragments.FragmentCreateAccount2;
import x44.codefox.com.easypay.Helper;
import x44.codefox.com.easypay.R;
import x44.codefox.com.easypay.UserClasses.User;

public class Register extends AppCompatActivity implements FragmentCreateAccount1.OnContinueClickedListener, FragmentCreateAccount2.OnRegisterClickListener {

    private Toolbar toolbar;
    private String thisMobilenum, thisPassword, thisSecretAnswer, thisEmail, thisFirstName,
            thisLastName, thisAddress, thisBirthDate, thisCreditCard, thisCartType;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mDatabaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        //init toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //show back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        //init firebase
        initFirebase();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                getSupportFragmentManager().popBackStack();
            } else {
                onBackPressed();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void setOnContinuedListener(String mobile_num, String password, String secret_answer, String email) {
        thisMobilenum = mobile_num;
        thisPassword = password;
        thisSecretAnswer = secret_answer;
        thisEmail = email;
    }

    @Override
    public void setOnRegisterListener(String firstName, String lastName, String address, String birthDate, String creditCardNo, String cardType) {
        thisFirstName = firstName;
        thisLastName = lastName;
        thisAddress = address;
        thisBirthDate = birthDate;
        thisCreditCard = creditCardNo;
        thisCartType = cardType;
        createUser();
    }

    private void initFirebase() {
        FirebaseApp.initializeApp(this);
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mDatabaseReference = mFirebaseDatabase.getReference();

    }

    private void createUser() {
        User user = new User();
        user.setMobileNum(thisMobilenum);
        user.setPassword(thisPassword);
        user.setSecretAnswer(thisSecretAnswer);
        user.setEmail(thisEmail);
        user.setFirstName(thisFirstName);
        user.setLastName(thisLastName);
        user.setAddress(thisAddress);
        user.setBirthdate(thisBirthDate);
        user.setCreditCardNo(thisCreditCard);
        user.setCardType(thisCartType);
        if (Helper.isNetworkAvailable(this, Helper.networks)) {
            mDatabaseReference.child("users").child(user.getMobileNum()).setValue(user);
            startActivity(new Intent(this, Landing.class));
        } else {
            Toast.makeText(this, "No network available", Toast.LENGTH_SHORT).show();
        }

    }
}
