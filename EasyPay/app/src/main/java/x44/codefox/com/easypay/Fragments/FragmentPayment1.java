package x44.codefox.com.easypay.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.UUID;

import io.realm.Realm;
import x44.codefox.com.easypay.BillingClass.BillingInformationRealm;
import x44.codefox.com.easypay.Helper;
import x44.codefox.com.easypay.Moneytextwatcher;
import x44.codefox.com.easypay.R;
import x44.codefox.com.easypay.RecordsClass.RecordsRealm;
import x44.codefox.com.easypay.UserClasses.UserRealm;

/**
 * Created by jesriel on 5/6/2017.
 */

public class FragmentPayment1 extends Fragment implements View.OnClickListener{
    private View mView;
    private Button easy_pay;
    private FragmentTransaction fragmentTransaction;
    private TextView company, cardNo, accountNo;
    private EditText amount, month, year, csc;
    private Realm realm;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mDatabaseReference;
    private UserRealm user;

    @Override

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Realm.getDefaultInstance();
        initFirebase();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_paybills, container, false);
        easy_pay = (Button) mView.findViewById(R.id.frag_paybills_easy_pay);
        easy_pay.setOnClickListener(this);
        company = (TextView) mView.findViewById(R.id.frag_paybills_company);
        cardNo = (TextView) mView.findViewById(R.id.frag_paybills_cardNo);
        amount = (EditText) mView.findViewById(R.id.frag_paybills_inputAmount);
        month = (EditText) mView.findViewById(R.id.frag_paybills_month);
        year = (EditText) mView.findViewById(R.id.frag_paybills_year);
        csc = (EditText) mView.findViewById(R.id.frag_paybills_csc);
        accountNo = (TextView) mView.findViewById(R.id.frag_paybills_accountNo);
        //aet Information
        amount.addTextChangedListener(new Moneytextwatcher(amount));
        setInformation();
        return mView;
    }

    private void setInformation() {
        BillingInformationRealm thisBill = realm.where(BillingInformationRealm.class)
                .equalTo("companyName", getArguments().getString("company")).findFirst();
        company.setText(thisBill.getCompanyName());
        accountNo.setText("Account No: " + thisBill.getAccountNo());
        user = realm.where(UserRealm.class).findFirst();
        cardNo.setText("Card No: " + user.getCreditCardNo());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.frag_paybills_easy_pay:
                if (isValidated()) goToConfirmationPage();
                break;
        }
    }

    private boolean isValidated() {
        if (TextUtils.isEmpty(month.getText().toString()) ||
                TextUtils.isEmpty(year.getText().toString()) ||
                TextUtils.isEmpty(csc.getText().toString()) ||
                TextUtils.isEmpty(amount.getText().toString())) {
            if (getView() != null) Snackbar.make(getView(),
                    "Please complete all fields", Snackbar.LENGTH_SHORT).show();
            return false;
        }

        return true;

    }


    private void goToConfirmationPage() {
        BillingInformationRealm thisBill = realm.where(BillingInformationRealm.class)
                .equalTo("companyName", getArguments().getString("company")).findFirst();
        String amountString = amount.getText().toString()
                .replace("₱", "")
                .replace(",","")
                .replace(".","");
        String trimmedString = amountString.substring(0, amountString.length() - 2);
        if (!amount.getText().toString().equals("")) {
            Float total = Float.parseFloat(thisBill.getAmountBalance()) - Float.parseFloat(trimmedString);
            if (total == 0.0 || total == 0 || total < 0) {
                mDatabaseReference.child("billing")
                        .child(thisBill.getCompanyNum())
                        .child(thisBill.getMobileNum())
                        .child(thisBill.getBillNo())
                        .child("amountBalance")
                        .setValue("0");
                mDatabaseReference.child("billing")
                        .child(thisBill.getCompanyNum())
                        .child(thisBill.getMobileNum())
                        .child(thisBill.getBillNo())
                        .child("status")
                        .setValue("paid");
                setPayment(thisBill, user.getCreditCardNo()
                        , month.getText().toString()
                        , year.getText().toString()
                        , csc.getText().toString()
                        , trimmedString);

            } else {
                mDatabaseReference.child("billing")
                        .child(thisBill.getCompanyNum())
                        .child(thisBill.getMobileNum())
                        .child(thisBill.getBillNo())
                        .child("amountBalance").setValue(total.toString());
            }

            gotoPayment2(amount.getText().toString(), company.getText().toString());
        }


    }

    private void gotoPayment2(String amount, String company) {
        //add data to bundle
        Bundle bundle = new Bundle();
        bundle.putString("amount", amount);
        bundle.putString("company", company);
        FragmentPayment2 payment = new FragmentPayment2();
        payment.setArguments(bundle);
        //go to another page
        fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.fragment_container_payment, payment);
        fragmentTransaction.commit();
    }


    private void setPayment(BillingInformationRealm thisBill,
                            String cardNo,
                            String expMonth,
                            String expYear,
                            String csc,
                            String amount) {

        String UUID_string = UUID.randomUUID().toString();

        realm.beginTransaction();
        RecordsRealm recordRealm = realm.where(RecordsRealm.class).equalTo("uuid", UUID_string).findFirst();
        if (recordRealm == null) {
            RecordsRealm record = realm.createObject(RecordsRealm.class, UUID_string);
            record.setAccountNo(thisBill.getAccountNo());
            record.setAddress(thisBill.getAddress());
            record.setAmount(amount);
            record.setCardNo(cardNo);
            record.setCompanyName(thisBill.getCompanyName());
            record.setCompanyNum(thisBill.getCompanyNum());
            record.setCsc(csc);
            record.setDatePaid(Helper.getCurrentDate());
            record.setExpMonth(expMonth);
            record.setExpYear(expYear);
            record.setFirstName(thisBill.getFirstName());
            record.setLastname(thisBill.getLastName());
            record.setMobileNum(thisBill.getMobileNum());
            record.setStatus(thisBill.getStatus());
        }
        realm.commitTransaction();

        mDatabaseReference.child("payment")
                .child(thisBill.getCompanyNum())
                .child(thisBill.getMobileNum())
                .child(thisBill.getBillNo())
                .child("uuid")
                .setValue(UUID_string);

        mDatabaseReference.child("payment")
                .child(thisBill.getCompanyNum())
                .child(thisBill.getMobileNum())
                .child(thisBill.getBillNo())
                .child("accountNo")
                .setValue(thisBill.getAccountNo());
        mDatabaseReference.child("payment")
                .child(thisBill.getCompanyNum())
                .child(thisBill.getMobileNum())
                .child(thisBill.getBillNo())
                .child("address")
                .setValue(thisBill.getAddress());
        mDatabaseReference.child("payment")
                .child(thisBill.getCompanyNum())
                .child(thisBill.getMobileNum())
                .child(thisBill.getBillNo())
                .child("companyName")
                .setValue(thisBill.getCompanyName());
        mDatabaseReference.child("payment")
                .child(thisBill.getCompanyNum())
                .child(thisBill.getMobileNum())
                .child(thisBill.getBillNo())
                .child("companyNum")
                .setValue(thisBill.getCompanyNum());
        mDatabaseReference.child("payment")
                .child(thisBill.getCompanyNum())
                .child(thisBill.getMobileNum())
                .child(thisBill.getBillNo())
                .child("datePaid")
                .setValue(Helper.getCurrentDate());
        mDatabaseReference.child("payment")
                .child(thisBill.getCompanyNum())
                .child(thisBill.getMobileNum())
                .child(thisBill.getBillNo())
                .child("firstName")
                .setValue(thisBill.getFirstName());
        mDatabaseReference.child("payment")
                .child(thisBill.getCompanyNum())
                .child(thisBill.getMobileNum())
                .child(thisBill.getBillNo())
                .child("lastName")
                .setValue(thisBill.getLastName());
        mDatabaseReference.child("payment")
                .child(thisBill.getCompanyNum())
                .child(thisBill.getMobileNum())
                .child(thisBill.getBillNo())
                .child("mobileNum")
                .setValue(thisBill.getMobileNum());
        mDatabaseReference.child("payment")
                .child(thisBill.getCompanyNum())
                .child(thisBill.getMobileNum())
                .child(thisBill.getBillNo())
                .child("status")
                .setValue("paid");
        mDatabaseReference.child("payment")
                .child(thisBill.getCompanyNum())
                .child(thisBill.getMobileNum())
                .child(thisBill.getBillNo())
                .child("cardNo")
                .setValue(cardNo);
        mDatabaseReference.child("payment")
                .child(thisBill.getCompanyNum())
                .child(thisBill.getMobileNum())
                .child(thisBill.getBillNo())
                .child("expMonth")
                .setValue(expMonth);
        mDatabaseReference.child("payment")
                .child(thisBill.getCompanyNum())
                .child(thisBill.getMobileNum())
                .child(thisBill.getBillNo())
                .child("expYear")
                .setValue(expYear);
        mDatabaseReference.child("payment")
                .child(thisBill.getCompanyNum())
                .child(thisBill.getMobileNum())
                .child(thisBill.getBillNo())
                .child("csc")
                .setValue(csc);
        mDatabaseReference.child("payment")
                .child(thisBill.getCompanyNum())
                .child(thisBill.getMobileNum())
                .child(thisBill.getBillNo())
                .child("amount")
                .setValue(amount);
    }

    private void initFirebase() {
        FirebaseApp.initializeApp(getContext());
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mDatabaseReference = mFirebaseDatabase.getReference();

    }


}
