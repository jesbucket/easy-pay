package x44.codefox.com.easypay;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by jesriel on 5/1/2017.
 */

public class Base extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        // The RealmActivity file will be located in Context.getFilesDir() with name "default.realm"
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder().build();
        Realm.setDefaultConfiguration(config);
    }
}
