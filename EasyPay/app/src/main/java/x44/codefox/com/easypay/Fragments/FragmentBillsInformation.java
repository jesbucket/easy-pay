package x44.codefox.com.easypay.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.percent.PercentRelativeLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import io.realm.Realm;
import x44.codefox.com.easypay.BillingClass.BillingInformationRealm;
import x44.codefox.com.easypay.CompaniesClass.CompaniesRealm;
import x44.codefox.com.easypay.R;

/**
 * Created by jesriel on 5/7/2017.
 */

public class FragmentBillsInformation extends Fragment implements View.OnClickListener {
    private View mView;
    private TextView mAccountNo, mFirstName, mLastName, mAddress, mAccountBalance, mDeadline;
    private Button mPaybill;
    private Realm realm;
    private OnClickPayBillListener onClickPaybillListener;
    private String companyName;
    private RelativeLayout noBillView;
    private PercentRelativeLayout withBillView;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Realm.getDefaultInstance();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_bills_information, container, false);
        mAccountNo = (TextView) mView.findViewById(R.id.fragment_billinginfo_acount);
        mFirstName = (TextView) mView.findViewById(R.id.fragment_billinginfo_first);
        mLastName = (TextView) mView.findViewById(R.id.fragment_billinginfo_last);
        mAddress = (TextView) mView.findViewById(R.id.fragment_billinginfo_address);
        mAccountBalance = (TextView) mView.findViewById(R.id.fragment_billinginfo_amount);
        mDeadline = (TextView) mView.findViewById(R.id.fragment_billinginfo_deadline);
        mPaybill = (Button) mView.findViewById(R.id.fragment_billinginfo_paybill);
        mPaybill.setOnClickListener(this);
        withBillView = (PercentRelativeLayout) mView.findViewById(R.id.frag_bills_information_withbill);
        noBillView = (RelativeLayout) mView.findViewById(R.id.frag_bills_information_nobill);

        companyName = getArguments().getString("company");
        BillingInformationRealm thisBill = realm.where(BillingInformationRealm.class)
                        .equalTo("companyName", getArguments()
                        .getString("company")).findFirst();

        if(thisBill != null){
            setInformation(thisBill.getAccountNo(),thisBill.getFirstName(),
                    thisBill.getLastName(), thisBill.getAddress(), thisBill.getAmountBalance(), thisBill.getDeadline());
        }else {
            noBillView.setVisibility(View.VISIBLE);
            withBillView.setVisibility(View.INVISIBLE);
        }


        return mView;
    }

    public void setInformation(String account, String first, String last, String address, String accountBal, String deadline){
        mAccountNo.setText(account);
        mFirstName.setText(first);
        mLastName.setText(last);
        mAddress.setText(address);
        mAccountBalance.setText(accountBal);
        mDeadline.setText(deadline);
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fragment_billinginfo_paybill:
                onClickPaybillListener.OnPayBill(companyName);
                break;
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onClickPaybillListener = (OnClickPayBillListener) context;
    }

    public interface OnClickPayBillListener{
        public void OnPayBill(String company);
    }
}
