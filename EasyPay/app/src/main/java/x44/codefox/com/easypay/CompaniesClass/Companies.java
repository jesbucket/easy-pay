package x44.codefox.com.easypay.CompaniesClass;

/**
 * Created by jesriel on 4/25/2017.
 */

public class Companies {


    private String id;
    private String  name, image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Companies() {

    }

    public Companies(String id, String name, String image) {

        this.id = id;
        this.name = name;
        this.image = image;
    }
}
