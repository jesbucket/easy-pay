package x44.codefox.com.easypay.AsyncTasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by jesriel on 5/5/2017.
 */

public class LoginAsync extends AsyncTask<Void, Void, Boolean> {
    HttpURLConnection urlc;
    SetOnConnectedListener setOnConnectedListener;
    Context context;

    public LoginAsync(Context context, SetOnConnectedListener setOnConnectedListener) {
        this.context = context;
        this.setOnConnectedListener = setOnConnectedListener;
    }


    @Override
    protected Boolean doInBackground(Void... params) {

        try {
            urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
            urlc.setRequestProperty("User-Agent", "Test");
            urlc.setRequestProperty("Connection", "close");
            urlc.setConnectTimeout(1500);
            urlc.connect();
            return urlc.getResponseCode() == 200;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    protected void onPostExecute(Boolean isConnected) {
        super.onPostExecute(isConnected);
        this.setOnConnectedListener.onConnectedToInternet(isConnected);

    }


}
