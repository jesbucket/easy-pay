package x44.codefox.com.easypay.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import x44.codefox.com.easypay.Activities.Transaction;
import x44.codefox.com.easypay.R;

/**
 * Created by jesriel on 5/6/2017.
 */

public class FragmentPayment2 extends Fragment implements View.OnClickListener{
    private View mView;
    private Button continueBTN, view_records;
    private TextView amount, company;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_paybills_confirm, container, false);
        continueBTN = (Button) mView.findViewById(R.id.frag_paybills2_continue);
        continueBTN.setOnClickListener(this);
        view_records = (Button) mView.findViewById(R.id.frag_paybills2_records);
        view_records.setOnClickListener(this);
        amount = (TextView) mView.findViewById(R.id.textView10);
        company = (TextView) mView.findViewById(R.id.textView11);

        //init datas
        setData();
        return mView;
    }

    private void setData() {
        Bundle bundle = this.getArguments();
        if(bundle != null){
            amount.setText(getArguments().getString("amount"));
            company.setText(getArguments().getString("company"));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.frag_paybills2_continue:
                startActivity(new Intent(getContext(), Transaction.class));
                break;
            case R.id.frag_paybills2_records:
                Intent intent = new Intent(getContext(), Transaction.class);
                intent.putExtra("fragment","records");
                startActivity(intent);
                break;
        }
    }
}
