package x44.codefox.com.easypay;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import java.lang.ref.WeakReference;
import java.math.BigDecimal;
import java.text.NumberFormat;

/**
 * Created by jesriel on 5/22/2017.
 */

public class Moneytextwatcher implements TextWatcher {
    private final WeakReference<EditText> editTextWeakReference;

    public Moneytextwatcher(EditText editTextWeakReference) {
        this.editTextWeakReference = new WeakReference<EditText>(editTextWeakReference);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        EditText editText = editTextWeakReference.get();
        if (editText == null) return;
        String s = editable.toString();
        editText.removeTextChangedListener(this);
        String cleanString = s.toString().replaceAll("[$,.]", "");
        String cleanerString = cleanString.replaceAll("[₱,.]", "");
        BigDecimal parsed = new BigDecimal(cleanerString).setScale(2, BigDecimal.ROUND_FLOOR).divide(new BigDecimal(100), BigDecimal.ROUND_FLOOR);
        String formatted = NumberFormat.getCurrencyInstance().format(parsed);
        String anotherFormat = formatted.replace("$", "₱");
        editText.setText(anotherFormat);
        editText.setSelection(formatted.length());
        editText.addTextChangedListener(this);
    }
}
