package x44.codefox.com.easypay;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import com.google.firebase.database.DatabaseReference;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.util.Date;
import java.util.Random;

/**
 * Created by jesriel on 4/25/2017.
 */

public class Helper {
    public static String [] companies = new String []{"company1", "company2"};
    private static final String ALLOWED_CHARACTERS ="0123456789qwertyuiopasdfghjklzxcvbnm";
    public static String getRandomString(final int sizeOfRandomString)
    {
        final Random random=new Random();
        final StringBuilder sb=new StringBuilder(sizeOfRandomString);
        for(int i=0;i<sizeOfRandomString;++i)
            sb.append(ALLOWED_CHARACTERS.charAt(random.nextInt(ALLOWED_CHARACTERS.length())));
        return sb.toString();
    }
    public static DatabaseReference mDataCompany(DatabaseReference mDatabasereference,String company, String mobileNum){
        return mDatabasereference.child("billing").child(company).child(mobileNum);
    }
    public static DatabaseReference mDataRecords(DatabaseReference mDatabasereference,String company, String mobileNum){
        return mDatabasereference.child("payment").child(company).child(mobileNum);
    }
    public static int[] networks = {ConnectivityManager.TYPE_BLUETOOTH,
            ConnectivityManager.TYPE_DUMMY,
            ConnectivityManager.TYPE_ETHERNET,
            ConnectivityManager.TYPE_MOBILE,
            ConnectivityManager.TYPE_MOBILE_DUN,
            ConnectivityManager.TYPE_VPN,
            ConnectivityManager.TYPE_WIFI,
            ConnectivityManager.TYPE_WIMAX};

    public static boolean isNetworkAvailable(Context context, int[] networkTypes) {
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            for (int networkType : networkTypes) {
                NetworkInfo netInfo = cm.getNetworkInfo(networkType);
                if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED) {
                    return true;
                }
            }
        } catch (Exception e) {
            return false;
        }
        return false;
    }


    public static boolean emailValidator(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static String getCurrentDate(){
        return String.valueOf(DateFormat.getDateTimeInstance().format(new Date()));
    }


}
