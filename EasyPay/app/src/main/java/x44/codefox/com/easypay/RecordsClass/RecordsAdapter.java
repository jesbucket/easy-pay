package x44.codefox.com.easypay.RecordsClass;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;
import x44.codefox.com.easypay.BillingClass.BillingAdapter;
import x44.codefox.com.easypay.BillingClass.BillingInformationRealm;
import x44.codefox.com.easypay.R;

/**
 * Created by jesriel on 5/21/2017.
 */

public class RecordsAdapter extends RealmRecyclerViewAdapter<RecordsRealm, RecordsAdapter.RecordsViewholder> {

    public RecordsAdapter(@Nullable OrderedRealmCollection<RecordsRealm> data, boolean autoUpdate) {
        super(data, autoUpdate);
    }


    @Override
    public RecordsAdapter.RecordsViewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_records, parent, false);
        return new RecordsAdapter.RecordsViewholder(view);
    }

    @Override
    public void onBindViewHolder(RecordsViewholder holder, int position) {
        RecordsRealm recordsRealm = getItem(position);
        if(recordsRealm != null){
            holder.companyName.setText(recordsRealm.getCompanyName());
            holder.datePaid.setText(recordsRealm.getDatePaid());
            holder.amountPaid.setText("P" + recordsRealm.getAmount());
        }

    }

    public class RecordsViewholder extends RecyclerView.ViewHolder {
        public TextView companyName, datePaid, amountPaid;
        public LinearLayout view;

        public RecordsViewholder(View itemView) {
            super(itemView);
            companyName = (TextView) itemView.findViewById(R.id.view_record_company);
            datePaid = (TextView) itemView.findViewById(R.id.view_record_deadline);
            amountPaid = (TextView) itemView.findViewById(R.id.view_record_currentBalance);
            view = (LinearLayout) itemView.findViewById(R.id.view_record_layout);
        }
    }
}
