package x44.codefox.com.easypay;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by jesriel on 5/1/2017.
 */

public class ConfigRealm extends RealmObject {
    @PrimaryKey
    private String configMain;
    private boolean isLoggedIn;

    public ConfigRealm(boolean isLoggedIn, String configMain) {
        this.isLoggedIn = isLoggedIn;
        this.configMain = configMain;
    }

    public ConfigRealm() {
    }


    public boolean isLoggedIn() {
        return isLoggedIn;
    }
    public void setLoggedIn(boolean loggedIn) {isLoggedIn = loggedIn;}
    public String getConfigMain() {return configMain;}
    public void setConfigMain(String configMain) {
        this.configMain = configMain;
    }
}
