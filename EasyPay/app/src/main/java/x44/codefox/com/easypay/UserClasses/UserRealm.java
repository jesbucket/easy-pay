package x44.codefox.com.easypay.UserClasses;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by jesriel on 5/1/2017.
 */

public class UserRealm extends RealmObject {
    @PrimaryKey
    private String mobileNum;
    private String password, secretAnswer, email, firstName, lastName, address, birthdate, creditCardNo, cardType;
    public UserRealm() {
    }

    public String getMobileNum() {
        return mobileNum;
    }

    public void setMobileNum(String mobileNum) {
        this.mobileNum = mobileNum;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSecretAnswer() {
        return secretAnswer;
    }

    public void setSecretAnswer(String secretAnswer) {
        this.secretAnswer = secretAnswer;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getCreditCardNo() {
        return creditCardNo;
    }

    public void setCreditCardNo(String creditCardNo) {
        this.creditCardNo = creditCardNo;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public UserRealm(String mobileNum, String password, String secretAnswer, String email, String firstName, String lastName,
                     String address, String birthdate, String creditCardNo, String cardType) {
        this.mobileNum = mobileNum;
        this.password = password;
        this.secretAnswer = secretAnswer;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.birthdate = birthdate;
        this.creditCardNo = creditCardNo;
        this.cardType = cardType;
    }
}
