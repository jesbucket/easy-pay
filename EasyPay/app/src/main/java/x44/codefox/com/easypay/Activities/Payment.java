package x44.codefox.com.easypay.Activities;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import x44.codefox.com.easypay.Fragments.FragmentBillingInfo;
import x44.codefox.com.easypay.Fragments.FragmentBillsInformation;
import x44.codefox.com.easypay.Fragments.FragmentCreateAccount1;
import x44.codefox.com.easypay.Fragments.FragmentCreateAccount2;
import x44.codefox.com.easypay.Fragments.FragmentPayment1;
import x44.codefox.com.easypay.R;

public class Payment extends AppCompatActivity implements FragmentBillsInformation.OnClickPayBillListener {

    private Toolbar toolbar;
    private FragmentTransaction fragmentTransaction;
    private FragmentBillsInformation fragmentBillsInformation = new FragmentBillsInformation();
    private String companyName;
    private Bundle bundle = new Bundle();
    private FragmentPayment1 fragmentPayment1 = new FragmentPayment1();
        @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        try{
            companyName = getIntent().getStringExtra("billing");
            //pass data to fragment
            bundle.putString("company", companyName);
            fragmentBillsInformation.setArguments(bundle);
            fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(R.id.fragment_container_payment, fragmentBillsInformation);
            fragmentTransaction.commit();

        }catch (Exception e){}

        //init toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //show back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);



    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                getSupportFragmentManager().popBackStack();
            } else {
                onBackPressed();
            }
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void OnPayBill(String company) {
        //set bundle
        Bundle bundle = new Bundle();
        bundle.putString("company", company);
        fragmentPayment1.setArguments(bundle);

        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container_payment, fragmentPayment1);
        fragmentTransaction.commit();
    }
}
