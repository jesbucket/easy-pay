package x44.codefox.com.easypay.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import x44.codefox.com.easypay.R;

/**
 * Created by jesriel on 4/22/2017.
 */

public class FragmentCreateAccount2 extends Fragment implements View.OnClickListener {

    private View mView;
    private Button register;
    private EditText input_firstName, input_lastName, input_address, input_birthdate, input_creditCardNo;
    private Spinner input_cardType;
    private String firstName, lastName, address, birthDate, creditCardNo, cardtType;
    private OnRegisterClickListener onRegisterClickListener;
    private boolean isValidated;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_create_account2, container, false);
        register = (Button) mView.findViewById(R.id.fragment_create_register);
        register.setOnClickListener(this);
        input_firstName = (EditText) mView.findViewById(R.id.fragment_create_first);
        input_lastName = (EditText) mView.findViewById(R.id.fragment_create_last);
        input_address = (EditText) mView.findViewById(R.id.fragment_create_address);
        input_birthdate = (EditText) mView.findViewById(R.id.fragment_create_birth);
        input_creditCardNo = (EditText) mView.findViewById(R.id.fragment_create_credit);
        input_cardType = (Spinner) mView.findViewById(R.id.fragment_create_type);
        return mView;
    }

    private void getInputs() {
        firstName = input_firstName.getText().toString();
        lastName = input_lastName.getText().toString();
        address = input_address.getText().toString();
        birthDate = input_birthdate.getText().toString();
        creditCardNo = input_creditCardNo.getText().toString();
        cardtType = input_cardType.getSelectedItem().toString();
    }

    private void validateRegister() {
        if (TextUtils.isEmpty(firstName) || TextUtils.isEmpty(lastName)) {
            Snackbar snackbar = Snackbar.make(getView(), "Input a valid name", Snackbar.LENGTH_SHORT);
            snackbar.show();
            isValidated = false;
        } else if (TextUtils.isEmpty(address)) {
            Snackbar snackbar = Snackbar.make(getView(), "Input a valid address", Snackbar.LENGTH_SHORT);
            snackbar.show();
            isValidated = false;
        } else if (TextUtils.isEmpty(birthDate)) {
            Snackbar snackbar = Snackbar.make(getView(), "Your birthdate is required", Snackbar.LENGTH_SHORT);
            snackbar.show();
            isValidated = false;
        } else if (TextUtils.isEmpty(creditCardNo)) {
            Snackbar snackbar = Snackbar.make(getView(), "Input your correct crdit card number", Snackbar.LENGTH_SHORT);
            snackbar.show();
            isValidated = false;
        } else {
            isValidated = true;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_create_register:
                getInputs();
                validateRegister();
                if (isValidated) {
                    onRegisterClickListener.setOnRegisterListener(firstName, lastName, address, birthDate, creditCardNo, cardtType);
                }
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onRegisterClickListener = (OnRegisterClickListener) context;
    }

    public interface OnRegisterClickListener {
        public void setOnRegisterListener(String firstName, String lastName, String address, String birthDate, String creditCardNo, String cardType);
    }
}
