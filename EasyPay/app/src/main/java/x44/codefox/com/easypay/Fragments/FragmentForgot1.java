package x44.codefox.com.easypay.Fragments;

import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import x44.codefox.com.easypay.CompaniesClass.Companies;
import x44.codefox.com.easypay.CompaniesClass.CompaniesRealm;
import x44.codefox.com.easypay.EmailSender.GmailSender;
import x44.codefox.com.easypay.R;
import x44.codefox.com.easypay.UserClasses.User;

import static x44.codefox.com.easypay.Helper.getRandomString;

/**
 * Created by jesriel on 4/22/2017.
 */

public class FragmentForgot1 extends Fragment implements View.OnClickListener {

    private View mView;
    private Button reset;
    private EditText inputEmail;
    private FragmentTransaction fragmentTransaction;
    private GmailSender gmailSender;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mDatabaseReference;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        allowSrictPolicy();
        initFirebase();
    }

    public void load_users(final String email) {
        mDatabaseReference.child("users").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snaps : dataSnapshot.getChildren()) {
                    User user = snaps.getValue(User.class);
                    if (user.getEmail().equals(email)) {
                        Log.d("test", user.getEmail() + "email user " + email + " :emailString");
                        gmailSender = new GmailSender("noreply.easypay@gmail.com", "123@ABCabc", getContext());
                        try {
                            String newPassword = getRandomString(5);
                            mDatabaseReference.child("users")
                                    .child(user.getMobileNum())
                                    .child("password")
                                    .setValue(newPassword);
                            gmailSender.sendMail("Password change",
                                    "Your password is change to " + newPassword +". Thank you for subscribing to Easy Pay",
                                    "noreply.easypay@gmail.com",
                                    email);
                            fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                            fragmentTransaction.hide(FragmentForgot1.this);
                            fragmentTransaction.add(R.id.fragment_conatainer_forgot, new FragmentForgot2(), "forgot1");
                            fragmentTransaction.addToBackStack("forgot1");
                            fragmentTransaction.commit();


                        } catch (Exception e) {
                            e.printStackTrace();
                        }



                    }
                }

            }


            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_reset_password, container, false);
        reset = (Button) mView.findViewById(R.id.fragment_reset_resetPassword);
        reset.setOnClickListener(this);
        inputEmail = (EditText) mView.findViewById(R.id.email);
        return mView;
    }

    private void initFirebase() {
        FirebaseApp.initializeApp(getContext());
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mDatabaseReference = mFirebaseDatabase.getReference();

    }

    private void allowSrictPolicy() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_reset_resetPassword:
                load_users(inputEmail.getText().toString());


                break;
        }
    }
}
