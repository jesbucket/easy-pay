package x44.codefox.com.easypay.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import io.realm.Realm;
import io.realm.RealmResults;
import x44.codefox.com.easypay.Activities.Forgot;
import x44.codefox.com.easypay.Activities.Register;
import x44.codefox.com.easypay.Activities.Transaction;
import x44.codefox.com.easypay.AsyncTasks.LoginAsync;
import x44.codefox.com.easypay.AsyncTasks.SetOnConnectedListener;
import x44.codefox.com.easypay.ConfigRealm;
import x44.codefox.com.easypay.Helper;
import x44.codefox.com.easypay.R;
import x44.codefox.com.easypay.UserClasses.User;
import x44.codefox.com.easypay.UserClasses.UserRealm;

/**
 * Created by jesriel on 4/22/2017.
 */

public class FragmentLanding extends Fragment implements View.OnClickListener, SetOnConnectedListener {

    private View mView;
    private Button login, register;
    private TextView forgot;
    private EditText input_mobile, input_password;
    private String mobile, password;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mDatabaseReference;
    private Realm realm;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Realm.getDefaultInstance();
        initFirebase();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_landing, container, false);
        login = (Button) mView.findViewById(R.id.fragment_landing_login);
        register = (Button) mView.findViewById(R.id.fragment_landing_register);
        forgot = (TextView) mView.findViewById(R.id.fragment_landing_forgot);
        //set click listerners
        login.setOnClickListener(this);
        register.setOnClickListener(this);
        forgot.setOnClickListener(this);
        //init edittexts
        input_mobile = (EditText) mView.findViewById(R.id.fragment_landing_mobile);
        input_password = (EditText) mView.findViewById(R.id.fragment_landing_password);
        return mView;
    }

    private void getInputs() {
        mobile = input_mobile.getText().toString();
        password = input_password.getText().toString();
    }

    private void initFirebase() {
        FirebaseApp.initializeApp(getContext());
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mDatabaseReference = mFirebaseDatabase.getReference();

    }

    private void validateogin() {
        DatabaseReference databaseReference = mDatabaseReference.child("users").child(mobile);
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                if (user == null) {
                    Snackbar snackbar = Snackbar.make(getView(), "Invalid credentials", Snackbar.LENGTH_SHORT);
                    snackbar.show();
                } else {
                    if (password.equals(user.getPassword())) {
                        realm.beginTransaction();
                        UserRealm userRealm1 = realm.createObject(UserRealm.class, user.getMobileNum());
                        userRealm1.setPassword(user.getPassword());
                        userRealm1.setSecretAnswer(user.getSecretAnswer());
                        userRealm1.setEmail(user.getEmail());
                        userRealm1.setFirstName(user.getFirstName());
                        userRealm1.setLastName(user.getLastName());
                        userRealm1.setAddress(user.getAddress());
                        userRealm1.setBirthdate(user.getBirthdate());
                        userRealm1.setCreditCardNo(user.getCreditCardNo());
                        userRealm1.setCardType(user.getCardType());
                        ConfigRealm configRealm = realm.where(ConfigRealm.class).equalTo("configMain", "config_main").findFirst();
                        configRealm.setLoggedIn(true);
                        realm.commitTransaction();
                        Toast.makeText(getContext(), "Successfully logged in", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getContext(), Transaction.class));
                    } else {
                        Snackbar snackbar = Snackbar.make(getView(), "Invalid credentials", Snackbar.LENGTH_SHORT);
                        snackbar.show();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_landing_login:
                getInputs();
                if (TextUtils.isEmpty(mobile) || TextUtils.isEmpty(password)) {
                    Snackbar.make(getView(), "Invalid credentials", Snackbar.LENGTH_SHORT).show();
                } else {
                    new LoginAsync(getContext(), this).execute();
                }
                break;
            case R.id.fragment_landing_register:
                startActivity(new Intent(getActivity(), Register.class));
                break;
            case R.id.fragment_landing_forgot:
                startActivity(new Intent(getContext(), Forgot.class));
                break;
        }
    }


    @Override
    public void onConnectedToInternet(boolean connection) {
        if (connection) {
            validateogin();
        } else {
            Snackbar snackbar = Snackbar.make(getView(), "No internet conenction", Snackbar.LENGTH_SHORT);
            snackbar.show();
        }

    }
}
