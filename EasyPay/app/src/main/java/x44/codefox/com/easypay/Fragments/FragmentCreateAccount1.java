package x44.codefox.com.easypay.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import x44.codefox.com.easypay.Helper;
import x44.codefox.com.easypay.R;

/**
 * Created by jesriel on 4/22/2017.
 */

public class FragmentCreateAccount1 extends Fragment implements View.OnClickListener {


    private View mView;
    private Button continueBTN;
    private FragmentTransaction fragmentTransaction;
    private OnContinueClickedListener onContinueClickedListener;
    private EditText input_mobile, input_password, input_secret, input_email;
    private String mobileNum, password, secretAnswer, email;
    private boolean isValidated;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_create_account1, container, false);
        continueBTN = (Button) mView.findViewById(R.id.fragment_create_continue);
        continueBTN.setOnClickListener(this);
        input_mobile = (EditText) mView.findViewById(R.id.fragment_create_no);
        input_password = (EditText) mView.findViewById(R.id.fragment_create_pass);
        input_secret = (EditText) mView.findViewById(R.id.fragment_create_answer);
        input_email = (EditText) mView.findViewById(R.id.fragment_create_email);
        return mView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fragment_create_continue:
                getInputs();
                validateSignup();
                if(isValidated){
                    onContinueClickedListener.setOnContinuedListener(mobileNum, password, secretAnswer, email);
                    fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
                    fragmentTransaction.hide(FragmentCreateAccount1.this);
                    fragmentTransaction.add(R.id.fragment_conatainer_register, new FragmentCreateAccount2(), "account1");
                    fragmentTransaction.addToBackStack("account1");
                    fragmentTransaction.commit();
                }
                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onContinueClickedListener = (OnContinueClickedListener) context;
    }
    private void getInputs(){
        mobileNum = input_mobile.getText().toString();
        password = input_password.getText().toString();
        secretAnswer = input_secret.getText().toString();
        email = input_email.getText().toString();
    }

    private void validateSignup() {
        if(TextUtils.isEmpty(mobileNum)){
            Snackbar snackbar = Snackbar.make(getView() , "Mobile number is required", Snackbar.LENGTH_SHORT);
            snackbar.show();
            isValidated = false;
        }else if(TextUtils.isEmpty(password)){
            Snackbar snackbar = Snackbar.make(getView() , "Your password is required", Snackbar.LENGTH_SHORT);
            snackbar.show();
            isValidated = false;
        }else if(TextUtils.isEmpty(secretAnswer)){
            Snackbar snackbar = Snackbar.make(getView() , "Secret answer is required", Snackbar.LENGTH_SHORT);
            snackbar.show();
            isValidated = false;
        }else if(TextUtils.isEmpty(email) || !Helper.emailValidator(email)){
            Snackbar snackbar = Snackbar.make(getView() , "Input a valid email", Snackbar.LENGTH_SHORT);
            snackbar.show();
            isValidated = false;
        }else {
            isValidated = true;
        }

    }

    public interface OnContinueClickedListener{
        public void setOnContinuedListener(String mobile_num, String password, String secret_answer, String email);
    }
}
