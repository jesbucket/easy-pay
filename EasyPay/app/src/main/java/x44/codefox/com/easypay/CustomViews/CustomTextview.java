package x44.codefox.com.easypay.CustomViews;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.support.v4.widget.TextViewCompat;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by jesriel on 4/21/2017.
 */

public class CustomTextview extends android.support.v7.widget.AppCompatTextView {
    public CustomTextview(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public CustomTextview(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);

    }

    public CustomTextview(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = FontCache.getTypeface("Camingo.otf", context);
        setTypeface(customFont);
    }

}
