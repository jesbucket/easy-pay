package x44.codefox.com.easypay.Activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import io.realm.Realm;
import io.realm.RealmResults;
import x44.codefox.com.easypay.CompaniesClass.CompaniesRealm;
import x44.codefox.com.easypay.ConfigRealm;
import x44.codefox.com.easypay.Fragments.FragmentAccountInfo;
import x44.codefox.com.easypay.Fragments.FragmentBillingInfo;
import x44.codefox.com.easypay.R;
import x44.codefox.com.easypay.UserClasses.UserRealm;
import x44.codefox.com.easypay.ViewPagerAdapter;

public class Account extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ViewPagerAdapter viewPagerAdapter;
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        //init realm
        realm = Realm.getDefaultInstance();
        //init toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //init navigation view
        navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);
        //init drawer layout
        drawerLayout = (DrawerLayout) findViewById(R.id.account_drawer);
        //init actionbar toggle
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        //init tablayout and viewpager
        tabLayout = (TabLayout) findViewById(R.id.account_tablayout);
        viewPager = (ViewPager) findViewById(R.id.account_viewpager);
        //init view pager adapter
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        //add viepager arraylist with fragments
        viewPagerAdapter.addFragments(new FragmentAccountInfo(),"Account Info");
        viewPagerAdapter.addFragments(new FragmentBillingInfo(),"Billing Info");
        //pass viewpageradapter to viewpager
        viewPager.setAdapter(viewPagerAdapter);
        //pass the viewpagers to tablayout to be inflated
        tabLayout.setupWithViewPager(viewPager);


    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getTitle().toString()) {
            case "Companies":
                startActivity(new Intent(this, Transaction.class));
                break;
            case "Records":
                Intent i = new Intent(this, Transaction.class);
                i.putExtra("fragment", "records");
                startActivity(i);
                break;
            case "Bills":
                Intent intent = new Intent(this, Transaction.class);
                intent.putExtra("fragment", "bills");
                startActivity(intent);
                break;
            case "Sign out":
                realm.beginTransaction();
                ConfigRealm configRealm = realm.where(ConfigRealm.class).equalTo("configMain", "config_main").findFirst();
                configRealm.setLoggedIn(false);
                RealmResults<UserRealm> user = realm.where(UserRealm.class).findAll();
                RealmResults<CompaniesRealm> allCompanies = realm.where(CompaniesRealm.class).findAll();
                allCompanies.deleteAllFromRealm();
                user.deleteAllFromRealm();
                realm.commitTransaction();
                startActivity(new Intent(this, Starting.class));
                Toast.makeText(this, "Succesfully Logout", Toast.LENGTH_SHORT).show();
                break;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(Intent.ACTION_MAIN);
        i.addCategory(Intent.CATEGORY_HOME);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }

    public void forceCrash() {
        throw new RuntimeException("This is a crash");
    }

}
