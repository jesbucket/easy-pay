package x44.codefox.com.easypay.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import x44.codefox.com.easypay.Activities.Landing;
import x44.codefox.com.easypay.R;

/**
 * Created by jesriel on 4/22/2017.
 */

public class FragmentForgot2 extends Fragment implements View.OnClickListener{

    private View mView;
    private Button continueBTN;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_reset_sent, container, false);
        continueBTN = (Button) mView.findViewById(R.id.fragment_forgot_continue);
        continueBTN.setOnClickListener(this);
        return mView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fragment_forgot_continue:
                startActivity(new Intent(getActivity(), Landing.class));
                break;
        }
    }
}
