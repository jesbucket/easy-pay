package x44.codefox.com.easypay.CompaniesClass;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.storage.StorageReference;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;
import x44.codefox.com.easypay.Activities.Payment;
import x44.codefox.com.easypay.Firebase.FirebaseImageLoader;
import x44.codefox.com.easypay.R;

/**
 * Created by jesriel on 4/25/2017.
 */

public class CompaniesAdapter extends RealmRecyclerViewAdapter<CompaniesRealm, CompaniesAdapter.ViewHolder> {

    private Context context;
    private StorageReference storageReference;
    private ClickListener clickListener;


    public CompaniesAdapter(@Nullable OrderedRealmCollection<CompaniesRealm> data, boolean autoUpdate, Context context,
                            StorageReference storageReference) {
        super(data, autoUpdate);
        this.context = context;
        this.storageReference = storageReference;
    }

    @Override
    public CompaniesAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_company, viewGroup, false);
        return new CompaniesAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        CompaniesRealm companiesRealm = getItem(position);
        holder.title.setText(companiesRealm.getName());
        holder.img.setScaleType(ImageView.ScaleType.FIT_CENTER);
        Glide.with(context)
                .using(new FirebaseImageLoader())
                .load(reference(companiesRealm.getImage()))
                .into(holder.img);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(clickListener != null){
                    clickListener.ItemClicked(holder.title.getText().toString(), position);
                }
            }
        });


    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void ItemClicked(String v, int position);
    }

    private StorageReference reference(String url) {
        return storageReference.child(url);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private ImageView img;

        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.company_name);
            img = (ImageView) itemView.findViewById(R.id.circle);
        }
    }


}
