package x44.codefox.com.easypay.BillingClass;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;
import x44.codefox.com.easypay.Activities.Payment;
import x44.codefox.com.easypay.R;

/**
 * Created by jesriel on 5/7/2017.
 */

public class BillingAdapter extends RealmRecyclerViewAdapter<BillingInformationRealm, BillingAdapter.BillingViewholder> {
    private Context context;
    public BillingAdapter(@Nullable OrderedRealmCollection<BillingInformationRealm> data, boolean autoUpdate, Context context) {
        super(data, autoUpdate);
        this.context = context;
    }


    @Override
    public BillingAdapter.BillingViewholder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_records, viewGroup, false);
        return new BillingAdapter.BillingViewholder(view);
    }


    @Override
    public void onBindViewHolder(final BillingViewholder holder, int position) {
        BillingInformationRealm billingRealm = getItem(position);
            holder.companyName.setText(billingRealm.getCompanyName());
            holder.deadline.setText(billingRealm.getDeadline());
            holder.currentBalance.setText("P" + billingRealm.getAmountBalance());
            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, Payment.class);
                    i.putExtra("billing",holder.companyName.getText().toString());
                    context.startActivity(i);
                }
            });


    }


    public class BillingViewholder extends RecyclerView.ViewHolder {
        public TextView companyName, deadline, currentBalance;
        public LinearLayout view;
        public BillingViewholder(View itemView) {
            super(itemView);
            companyName = (TextView) itemView.findViewById(R.id.view_record_company);
            deadline = (TextView) itemView.findViewById(R.id.view_record_deadline);
            currentBalance = (TextView) itemView.findViewById(R.id.view_record_currentBalance);
            view = (LinearLayout) itemView.findViewById(R.id.view_record_layout);
        }
    }

}
