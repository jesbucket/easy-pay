package x44.codefox.com.easypay.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import io.realm.Realm;
import x44.codefox.com.easypay.ConfigRealm;

/**
 * Created by jesriel on 5/4/2017.
 */

public class Starting extends AppCompatActivity {
    private Realm realm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Realm.getDefaultInstance();
        //check if logged in or not
        ConfigRealm configRealm_0 = realm.where(ConfigRealm.class).findFirst();
        if(configRealm_0 == null){
            realm.beginTransaction();
            ConfigRealm configRealm_01 = realm.createObject(ConfigRealm.class, "config_main");
            configRealm_01.setLoggedIn(false);
            realm.commitTransaction();
        }
        ConfigRealm configRealm = realm.where(ConfigRealm.class).findFirst();
        if(configRealm.isLoggedIn())startActivity(new Intent(this, Transaction.class));
        else startActivity(new Intent(this, Landing.class));
    }


}
