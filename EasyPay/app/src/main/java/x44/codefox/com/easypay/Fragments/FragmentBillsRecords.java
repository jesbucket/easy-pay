package x44.codefox.com.easypay.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import io.realm.Realm;
import io.realm.RealmResults;
import x44.codefox.com.easypay.BillingClass.BillingAdapter;
import x44.codefox.com.easypay.BillingClass.BillingInformation;
import x44.codefox.com.easypay.BillingClass.BillingInformationRealm;
import x44.codefox.com.easypay.Helper;
import x44.codefox.com.easypay.R;
import x44.codefox.com.easypay.UserClasses.UserRealm;

/**
 * Created by jesriel on 5/7/2017.
 */

public class FragmentBillsRecords extends Fragment {

    private View mView;
    private Realm realm;
    private RecyclerView recyclerView;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mDatabaseReference;
    private BillingAdapter billingAdapter;
    private TextView nobills;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Realm.getDefaultInstance();
        //inititalze firebase\
        initFirebase();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_bills, container, false);
        recyclerView = (RecyclerView) mView.findViewById(R.id.frag_bills_recycler);
        nobills = (TextView) mView.findViewById(R.id.frag_bills_nobills);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        initUnpaidBills();
        return mView;
    }

    private void setDataAdapter() {
        RealmResults<BillingInformationRealm> billingRealm = realm.where(BillingInformationRealm.class).findAll();
        if (billingRealm != null) {
            billingAdapter = new BillingAdapter(billingRealm, true, getContext());
            recyclerView.setAdapter(billingAdapter);
            recyclerView.setHasFixedSize(true);
        }
        RealmResults<BillingInformationRealm> billingInformationRealms = realm.where(BillingInformationRealm.class).equalTo("status", "paid").findAll();
        if(billingInformationRealms.size() == billingRealm.size()){
            recyclerView.setVisibility(View.INVISIBLE);
            nobills.setVisibility(View.VISIBLE);
        }
    }

    private void initUnpaidBills() {
        UserRealm user = realm.where(UserRealm.class).findFirst();
        for (int i = 0; i < Helper.companies.length; i++) {
            Helper.mDataCompany(mDatabaseReference, Helper.companies[i], user.getMobileNum()).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot data : dataSnapshot.getChildren()) {
                        BillingInformation billingInformation = data.getValue(BillingInformation.class);
                        BillingInformationRealm bill_0 = realm.where(BillingInformationRealm.class).equalTo("companyName", billingInformation.getCompanyName()).findFirst();
                        if (bill_0 == null) {
                            if (billingInformation.getStatus().equals("unpaid")) {
                                realm.beginTransaction();
                                BillingInformationRealm bill = realm.createObject(BillingInformationRealm.class, billingInformation.getCompanyName());
                                bill.setAccountNo(billingInformation.getAccountNo());
                                bill.setAddress(billingInformation.getAddress());
                                bill.setAmountBalance(billingInformation.getAmountBalance());
                                bill.setDeadline(billingInformation.getDeadline());
                                bill.setFirstName(billingInformation.getFirstName());
                                bill.setLastName(billingInformation.getLastName());
                                bill.setMobileNum(billingInformation.getMobileNum());
                                bill.setStatus(billingInformation.getStatus());
                                bill.setBillNo(billingInformation.getBillNo());
                                bill.setCompanyNum(billingInformation.getCompanyNum());
                                realm.commitTransaction();
                            }
                        } else {
                            realm.beginTransaction();
                            BillingInformationRealm myBill = realm.where(BillingInformationRealm.class).equalTo("companyName", billingInformation.getCompanyName()).findFirst();
                            if (myBill != null && myBill.getStatus().equals("unpaid")) {
                                myBill.setAccountNo(billingInformation.getAccountNo());
                                myBill.setAddress(billingInformation.getAddress());
                                myBill.setAmountBalance(billingInformation.getAmountBalance());
                                myBill.setDeadline(billingInformation.getDeadline());
                                myBill.setFirstName(billingInformation.getFirstName());
                                myBill.setLastName(billingInformation.getLastName());
                                myBill.setMobileNum(billingInformation.getMobileNum());
                                myBill.setStatus(billingInformation.getStatus());
                                myBill.setBillNo(billingInformation.getBillNo());
                                myBill.setCompanyNum(billingInformation.getCompanyNum());
                            }

                            if (myBill != null && myBill.getStatus().equals("paid"))
                                myBill.deleteFromRealm();
                            realm.commitTransaction();
                        }
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }


            });
        }
        setDataAdapter();
    }

    private void initFirebase() {
        FirebaseApp.initializeApp(getContext());
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mDatabaseReference = mFirebaseDatabase.getReference();

    }

}
