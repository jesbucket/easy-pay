package x44.codefox.com.easypay.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.StorageReference;

import io.realm.Realm;
import x44.codefox.com.easypay.R;
import x44.codefox.com.easypay.UserClasses.UserRealm;

public class Edit extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private EditText mobile, password, secretAnswer, email;
    private EditText firstName, lastName, address, birthdate, cardNo;
    private Spinner cardType;
    private Realm realm;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mDatabaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Realm.getDefaultInstance();
        initFirebase();
        try {
            switch (getIntent().getStringExtra("fragment")) {
                case "account":
                    setContentView(R.layout.activity_edit_account);
                    initAccountDatas();
                    break;
                case "billing":
                    setContentView(R.layout.activity_edit_billing);
                    initBillingDatas();
                    break;
            }

        } catch (Exception e) {
        }
        //init toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //show back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


    }

    private void initBillingDatas() {
        firstName = (EditText) findViewById(R.id.act_edit_billing_first);
        lastName = (EditText) findViewById(R.id.act_edit_billing_last);
        address = (EditText) findViewById(R.id.act_edit_billing_address);
        birthdate = (EditText) findViewById(R.id.act_edit_billing_birth);
        cardNo = (EditText) findViewById(R.id.act_edit_billing_credit);
        cardType = (Spinner) findViewById(R.id.act_edit_billing_type);
        findViewById(R.id.act_edit_billing_edit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateBillingInfo();
                startActivity(new Intent(getApplicationContext(), Account.class));
            }
        });


        UserRealm user = realm.where(UserRealm.class).findFirst();
        firstName.setText(user.getFirstName());
        lastName.setText(user.getLastName());
        address.setText(user.getAddress());
        birthdate.setText(user.getAddress());
        cardNo.setText(user.getCreditCardNo());
        if (user.getCardType().equals("Visa")) cardType.setSelection(0);
        else cardType.setSelection(1);
    }

    private void initAccountDatas() {
        mobile = (EditText) findViewById(R.id.act_edit_account_mobile);
        password = (EditText) findViewById(R.id.act_edit_account_pass);
        secretAnswer = (EditText) findViewById(R.id.act_edit_account_answer);
        email = (EditText) findViewById(R.id.act_edit_account__email);
        findViewById(R.id.act_edit_account_edit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateAccountInfo();
                startActivity(new Intent(getApplicationContext(), Account.class));
            }
        });

        UserRealm user = realm.where(UserRealm.class).findFirst();
        mobile.setText(user.getMobileNum());
        password.setText(user.getPassword());
        secretAnswer.setText(user.getSecretAnswer());
        email.setText(user.getEmail());
    }

    private void updateAccountInfo() {

        realm.beginTransaction();
        UserRealm user = realm.where(UserRealm.class).equalTo("mobileNum", mobile.getText().toString()).findFirst();
        user.setPassword(password.getText().toString());
        user.setSecretAnswer(secretAnswer.getText().toString());
        user.setEmail(email.getText().toString());
        realm.commitTransaction();

        UserRealm user_0 = realm.where(UserRealm.class).findFirst();
        mDatabaseReference.child("users").child(user_0.getMobileNum()).child("mobileNum").setValue(user_0.getMobileNum());
        mDatabaseReference.child("users").child(user_0.getMobileNum()).child("password").setValue(user_0.getPassword());
        mDatabaseReference.child("users").child(user_0.getMobileNum()).child("secretAnswer").setValue(user_0.getSecretAnswer());
        mDatabaseReference.child("users").child(user_0.getMobileNum()).child("email").setValue(user_0.getEmail());

    }

    private void updateBillingInfo() {
        realm.beginTransaction();

        UserRealm user = realm.where(UserRealm.class).findFirst();
        user.setFirstName(firstName.getText().toString());
        user.setLastName(lastName.getText().toString());
        user.setAddress(address.getText().toString());
        user.setBirthdate(birthdate.getText().toString());
        user.setCreditCardNo(cardNo.getText().toString());
        user.setCardType(cardType.getSelectedItem().toString());
        realm.copyToRealmOrUpdate(user);
        realm.commitTransaction();

        UserRealm user_0 = realm.where(UserRealm.class).findFirst();
        mDatabaseReference.child("users").child(user_0.getMobileNum()).child("firstName").setValue(user_0.getFirstName());
        mDatabaseReference.child("users").child(user_0.getMobileNum()).child("lastName").setValue(user_0.getLastName());
        mDatabaseReference.child("users").child(user_0.getMobileNum()).child("address").setValue(user_0.getAddress());
        mDatabaseReference.child("users").child(user_0.getMobileNum()).child("birthdate").setValue(user_0.getBirthdate());
        mDatabaseReference.child("users").child(user_0.getMobileNum()).child("creditCardNo").setValue(user_0.getCreditCardNo());
        mDatabaseReference.child("users").child(user_0.getMobileNum()).child("cardType").setValue(user_0.getCardType());

    }


    private void initFirebase() {
        FirebaseApp.initializeApp(this);
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mDatabaseReference = mFirebaseDatabase.getReference();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                getSupportFragmentManager().popBackStack();
            } else {
                onBackPressed();
            }
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.act_edit_account_edit:

                break;
        }
    }
}
