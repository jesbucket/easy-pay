package x44.codefox.com.easypay.AsyncTasks;

/**
 * Created by jesriel on 5/5/2017.
 */

public interface SetOnConnectedListener {
    public void onConnectedToInternet(boolean connection);
}
