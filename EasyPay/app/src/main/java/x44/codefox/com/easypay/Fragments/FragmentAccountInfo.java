package x44.codefox.com.easypay.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import io.realm.Realm;
import x44.codefox.com.easypay.Activities.Edit;
import x44.codefox.com.easypay.R;
import x44.codefox.com.easypay.UserClasses.UserRealm;

/**
 * Created by jesriel on 4/22/2017.
 */

public class FragmentAccountInfo extends Fragment implements View.OnClickListener {
    private View mView;
    private TextView mobile, password, secret_question, secret_answer, email;
    private ImageView mobile_edit, password_edit, secretQuestion_edit, secretAnswer_edit, email_edit;
    private Realm realm;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Realm.getDefaultInstance();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_account_info, container, false);
        mobile = (TextView) mView.findViewById(R.id.fragment_accountinfo_mobile);
        password = (TextView) mView.findViewById(R.id.fragment_accountinfo_password);
        secret_question = (TextView) mView.findViewById(R.id.fragment_accountinfo_secretquestion);
        secret_answer = (TextView) mView.findViewById(R.id.fragment_accountinfo_answer);
        email = (TextView) mView.findViewById(R.id.fragment_accountinfo_email);
        mobile_edit = (ImageView) mView.findViewById(R.id.imageView);
        mobile_edit.setOnClickListener(this);
        secretQuestion_edit = (ImageView) mView.findViewById(R.id.imageView1);
        secretQuestion_edit.setOnClickListener(this);
        password_edit = (ImageView) mView.findViewById(R.id.imageView2);
        password_edit.setOnClickListener(this);
        secretAnswer_edit = (ImageView) mView.findViewById(R.id.imageView3);
        secretAnswer_edit.setOnClickListener(this);
        email_edit = (ImageView) mView.findViewById(R.id.imageView4);
        email_edit.setOnClickListener(this);
        //set content of views
        initTexviewsData();
        return mView;
    }

    private void initTexviewsData() {
        UserRealm userRealm = realm.where(UserRealm.class).findFirst();
        mobile.setText(userRealm.getMobileNum());
        password.setText(userRealm.getPassword());
        secret_answer.setText(userRealm.getSecretAnswer());
        email.setText(userRealm.getEmail());
    }

    @Override
    public void onClick(View v) {
        Intent i = new Intent(getContext(), Edit.class);
        i.putExtra("fragment", "account");
        startActivity(i);
    }
}
