package x44.codefox.com.easypay.Activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import io.realm.Realm;
import io.realm.RealmResults;
import x44.codefox.com.easypay.BillingClass.BillingInformation;
import x44.codefox.com.easypay.BillingClass.BillingInformationRealm;
import x44.codefox.com.easypay.CompaniesClass.CompaniesRealm;
import x44.codefox.com.easypay.ConfigRealm;
import x44.codefox.com.easypay.Fragments.FragmentBillsRecords;
import x44.codefox.com.easypay.Fragments.FragmentCompany;
import x44.codefox.com.easypay.Fragments.FragmentRecords;
import x44.codefox.com.easypay.R;
import x44.codefox.com.easypay.RecordsClass.RecordsRealm;
import x44.codefox.com.easypay.UserClasses.UserRealm;


public class Transaction extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {


    private DrawerLayout drawerLayout;
    private NavigationView navigationView_1, navigationView_0;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private Toolbar toolbar;
    private FragmentTransaction fragmentTransaction;
    private FragmentCompany fragmentCompany = new FragmentCompany();
    private FragmentRecords fragmentRecords = new FragmentRecords();
    private FragmentBillsRecords fragmentBills = new FragmentBillsRecords();
    private Realm realm;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mDatabaseReference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction);
        //init firebase
        initFirebase();
        //init realm
        realm = Realm.getDefaultInstance();
        //init toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //init navigation view
        navigationView_0 = (NavigationView) findViewById(R.id.navigation_0);
        navigationView_0.setNavigationItemSelectedListener(this);
        navigationView_0.setItemIconTintList(null);
        //init navigation 2
        navigationView_1 = (NavigationView) findViewById(R.id.navigation_1);
        navigationView_1.setNavigationItemSelectedListener(this);
        navigationView_1.setItemIconTintList(null);

        //init drawer layout
        drawerLayout = (DrawerLayout) findViewById(R.id.transaction_drawer);
        //init actionbar toggle
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.drawer_open, R.string.drawer_close);
        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        //init fragment transaction
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.fragment_conatainer_transaction, fragmentCompany);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

        try {
            Intent intent = getIntent();
            if (getIntent() != null) {
                switch (intent.getStringExtra("fragment")) {
                    case "records":
                        fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.fragment_conatainer_transaction, fragmentRecords);
                        fragmentTransaction.commit();
                        break;
                    case "bills":
                        fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.fragment_conatainer_transaction, fragmentBills);
                        fragmentTransaction.commit();
                        break;
                }
            }
        } catch (Exception e) {}
    }





    private void initFirebase() {
        FirebaseApp.initializeApp(this);
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mDatabaseReference = mFirebaseDatabase.getReference();

    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getTitle().toString()) {
            case "Account":
                startActivity(new Intent(this, Account.class));
                break;
            case "Records":
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragment_conatainer_transaction, fragmentRecords);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                drawerLayout.closeDrawers();
                break;
            case "Companies":
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragment_conatainer_transaction, fragmentCompany);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                drawerLayout.closeDrawers();
                break;
            case "Bills":
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragment_conatainer_transaction, fragmentBills);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                drawerLayout.closeDrawers();
                break;
            case "Sign out":
                realm.beginTransaction();
                ConfigRealm configRealm = realm.where(ConfigRealm.class).equalTo("configMain", "config_main").findFirst();
                configRealm.setLoggedIn(false);
                RealmResults<UserRealm> user = realm.where(UserRealm.class).findAll();
                RealmResults<CompaniesRealm> allCompanies = realm.where(CompaniesRealm.class).findAll();
                RealmResults<BillingInformationRealm> allBills = realm.where(BillingInformationRealm.class).findAll();
                RealmResults<RecordsRealm> allRecords = realm.where(RecordsRealm.class).findAll();
                allCompanies.deleteAllFromRealm();
                user.deleteAllFromRealm();
                allBills.deleteAllFromRealm();
                allRecords.deleteAllFromRealm();
                realm.commitTransaction();
                startActivity(new Intent(this, Starting.class));
                Toast.makeText(this, "Succesfully Logout", Toast.LENGTH_SHORT).show();
                break;



        }
        return false;
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(Intent.ACTION_MAIN);
        i.addCategory(Intent.CATEGORY_HOME);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);

    }


}
