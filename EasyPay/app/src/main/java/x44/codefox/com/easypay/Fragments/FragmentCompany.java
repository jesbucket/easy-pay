package x44.codefox.com.easypay.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmResults;
import x44.codefox.com.easypay.Activities.Payment;
import x44.codefox.com.easypay.CompaniesClass.Companies;
import x44.codefox.com.easypay.CompaniesClass.CompaniesAdapter;
import x44.codefox.com.easypay.CompaniesClass.CompaniesRealm;
import x44.codefox.com.easypay.R;

/**
 * Created by jesriel on 4/22/2017.
 */

public class FragmentCompany extends Fragment implements CompaniesAdapter.ClickListener {

    private View mView;
    private RecyclerView recyclerView;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mDatabaseReference;
    private FirebaseStorage storage;
    private StorageReference storageReference;
    private Realm realm;
    private ProgressBar progressBar;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initFirebase();
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        realm = Realm.getDefaultInstance();

    }



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_company_list, container, false);
        recyclerView = (RecyclerView) mView.findViewById(R.id.fragment_companylist_recycler);
        progressBar = (ProgressBar) mView.findViewById(R.id.fragment_company_progress);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(), 3);
        recyclerView.setLayoutManager(layoutManager);
        //init companies
        loadCompanies_0();
        setAdapter();
        return mView;
    }


    public void loadCompanies_0() {
        mDatabaseReference.child("companies").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                CompaniesRealm company = realm.where(CompaniesRealm.class).findFirst();
                if (company == null) {
                    for (DataSnapshot snaps : dataSnapshot.getChildren()) {
                        realm.beginTransaction();
                        Companies companies = snaps.getValue(Companies.class);
                        CompaniesRealm companiesRealm = realm.createObject(CompaniesRealm.class);
                        companiesRealm.setId(companies.getId());
                        companiesRealm.setImage(companies.getImage());
                        companiesRealm.setName(companies.getName());
                        realm.commitTransaction();
                    }
                }

            }


            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void setAdapter() {
        RealmResults<CompaniesRealm> companiesRealms = realm.where(CompaniesRealm.class).findAll();
        if (companiesRealms != null) {
            CompaniesAdapter companiesAdapter = new CompaniesAdapter(companiesRealms, true, getContext(), storageReference);
            companiesAdapter.setClickListener(this);
            recyclerView.getRecycledViewPool().clear();
            recyclerView.setHasFixedSize(true);
            recyclerView.setAdapter(companiesAdapter);


        }
    }

    private void initFirebase() {
        FirebaseApp.initializeApp(getContext());
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mDatabaseReference = mFirebaseDatabase.getReference();

    }


    @Override
    public void ItemClicked(String v, int position) {
        Intent i = new Intent(getContext(), Payment.class);
        i.putExtra("billing", v);
        startActivity(i);
    }
}



