package x44.codefox.com.easypay.Fragments;

import android.icu.text.AlphabeticIndex;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmResults;
import x44.codefox.com.easypay.BillingClass.BillingAdapter;
import x44.codefox.com.easypay.BillingClass.BillingInformation;
import x44.codefox.com.easypay.BillingClass.BillingInformationRealm;
import x44.codefox.com.easypay.Helper;
import x44.codefox.com.easypay.R;
import x44.codefox.com.easypay.RecordsClass.Records;
import x44.codefox.com.easypay.RecordsClass.RecordsAdapter;
import x44.codefox.com.easypay.RecordsClass.RecordsRealm;
import x44.codefox.com.easypay.UserClasses.UserRealm;

/**
 * Created by jesriel on 4/22/2017.
 */

public class FragmentRecords extends Fragment {

    private View mView;
    private RecyclerView recyclerView;
    private Realm realm;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mDatabaseReference;
    private RecordsAdapter recordsAdapter;
    private TextView nopayment;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Realm.getDefaultInstance();
        initFirebase();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_records, container, false);
        recyclerView = (RecyclerView) mView.findViewById(R.id.frag_records_recyclerView);
        nopayment = (TextView) mView.findViewById(R.id.frag_records_nopayment);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        initRecords();
        return mView;
    }


    private void initRecords() {
        UserRealm user = realm.where(UserRealm.class).findFirst();
        for (int i = 0; i < Helper.companies.length; i++) {
            Helper.mDataRecords(mDatabaseReference, Helper.companies[i], user.getMobileNum()).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot data : dataSnapshot.getChildren()) {
                        Records records = data.getValue(Records.class);
                        realm.beginTransaction();
                        RecordsRealm recordRealm = realm.where(RecordsRealm.class).equalTo("uuid", records.getUuid()).findFirst();
                        if (recordRealm == null) {
                            RecordsRealm record = realm.createObject(RecordsRealm.class, records.getUuid());
                            record.setAccountNo(records.getAccountNo());
                            record.setAddress(records.getAddress());
                            record.setAmount(records.getAmount());
                            record.setCardNo(records.getCardNo());
                            record.setCompanyName(records.getCompanyName());
                            record.setCompanyNum(records.getCompanyNum());
                            record.setCsc(records.getCsc());
                            record.setDatePaid(records.getDatePaid());
                            record.setExpMonth(records.getExpMonth());
                            record.setExpYear(records.getExpYear());
                            record.setFirstName(records.getFirstName());
                            record.setLastname(records.getLastname());
                            record.setMobileNum(records.getMobileNum());
                            record.setStatus(records.getStatus());
                        }
                        realm.commitTransaction();

                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }


            });
        }
        setAdapter();


    }

    private void setAdapter() {
        RealmResults<RecordsRealm> records = realm.where(RecordsRealm.class).findAll();
        if (records.size() > 0)  {
            recordsAdapter = new RecordsAdapter(records, true);
            recyclerView.setAdapter(recordsAdapter);
            recyclerView.setHasFixedSize(true);
        }else  {
            nopayment.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.INVISIBLE);
        }


    }

    private void initFirebase() {
        FirebaseApp.initializeApp(getContext());
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mDatabaseReference = mFirebaseDatabase.getReference();

    }


}
