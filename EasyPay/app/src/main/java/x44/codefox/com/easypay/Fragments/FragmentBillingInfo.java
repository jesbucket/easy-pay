package x44.codefox.com.easypay.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import io.realm.Realm;
import x44.codefox.com.easypay.Activities.Edit;
import x44.codefox.com.easypay.R;
import x44.codefox.com.easypay.UserClasses.UserRealm;

/**
 * Created by jesriel on 4/22/2017.
 */

public class FragmentBillingInfo extends Fragment implements View.OnClickListener {
    private View mView;
    private TextView firstName, lastName, address, birthdate, cardNo, cardType;
    private ImageView image,image1, image2, image3, image4, image5;
    private Realm realm;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Realm.getDefaultInstance();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_billing_info, container, false);
        image = (ImageView) mView.findViewById(R.id.imageView);
        image.setOnClickListener(this);
        image1 = (ImageView) mView.findViewById(R.id.imageView1);
        image1.setOnClickListener(this);
        image2 = (ImageView) mView.findViewById(R.id.imageView2);
        image2.setOnClickListener(this);
        image3 = (ImageView) mView.findViewById(R.id.imageView3);
        image3.setOnClickListener(this);
        image4 = (ImageView) mView.findViewById(R.id.imageView4);
        image4.setOnClickListener(this);
        image5 = (ImageView) mView.findViewById(R.id.imageView5);
        image5.setOnClickListener(this);
        firstName = (TextView) mView.findViewById(R.id.fragment_billinginfo_first);
        lastName = (TextView) mView.findViewById(R.id.fragment_billinginfo_last);
        address = (TextView) mView.findViewById(R.id.fragment_billinginfo_address);
        birthdate = (TextView) mView.findViewById(R.id.fragment_billinginfo_birth);
        cardNo = (TextView) mView.findViewById(R.id.fragment_billinginfo_cardno);
        cardType = (TextView) mView.findViewById(R.id.fragment_billinginfo_cardtype);
        //init contents
        initTextviewsDatas();
        return mView;
    }

    private void initTextviewsDatas() {
        UserRealm userRealm = realm.where(UserRealm.class).findFirst();
        firstName.setText(userRealm.getFirstName());
        lastName.setText(userRealm.getLastName());
        address.setText(userRealm.getAddress());
        birthdate.setText(userRealm.getBirthdate());
        cardNo.setText(userRealm.getCreditCardNo());
        cardType.setText(userRealm.getCardType());
    }

    @Override
    public void onClick(View v) {
        Intent i = new Intent(getContext(), Edit.class);
        i.putExtra("fragment", "billing");
        startActivity(i);
    }
}
